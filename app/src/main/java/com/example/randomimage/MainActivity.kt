package com.example.randomimage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val images =
        arrayOf(
            R.mipmap.daft_punk,
            R.mipmap.led_zeppelin,
            R.mipmap.queen,
            R.mipmap.the_beatles,
            R.mipmap.pink_floyd
        )
    private lateinit var imageViews: List<ImageView>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        imageView1.setOnClickListener { randomImageView() }
        imageView2.setOnClickListener { randomImageView() }
        imageView3.setOnClickListener { randomImageView() }
        imageView4.setOnClickListener { randomImageView() }


    }

    private fun getRandomImage() = images[(images.indices).random()]


    private fun randomImageView() {
        imageViews = arrayListOf(imageView1, imageView2, imageView3, imageView4)
        val generated = imageViews[(imageViews.indices).random()]
        generated.setImageResource(getRandomImage())


    }


}




